/*
 * Problem.cpp
 *
 *  Created on: 24/mar/2016
 *      Author: pego
 */

#include "Problem.h"
#include <wchar.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


using namespace std;

class CryptoString{
public:
	CryptoString() : m_len(0), m_data(NULL){}

	void Init(const CryptoText *str, int len){
		m_len = len;
		m_data = new CryptoText[len];
		memcpy(m_data, str, len*sizeof(CryptoText));
	}

	~CryptoString(){
		if(m_data)
			delete [] m_data;
	}

	bool PatternMatching(const wchar_t *pattern, key &mapN2Char){
		//costruisce la mappa che dal pattern
		for(unsigned int i=0; i < m_len; i++){
			if(mapN2Char[m_data[i]] && mapN2Char[m_data[i]] != pattern[i])
				return false;
			mapN2Char[m_data[i]] = pattern[i];
		}

		char mapCH2N[(L'z' - L'a') + (L'9' - L'0') + 6]; //usa anche 6 lettere accentate
		memset(mapCH2N, 0, sizeof(mapCH2N));
		/*
		 * abcdefghijklmnopqrstuvwxyz0123456789àèéìòù
		 */

		for(unsigned int i=0; i < m_len; i++){
			int pos = 0;
			if(pattern[i] >= L'a' && pattern[i] <= L'z')
				pos = pattern[i] - L'a';
			else if(pattern[i] >= L'0' && pattern[i] <= L'9')
				pos = pattern[i] - L'0' + (L'z' - L'a') + 1;
			else{
				int offsetWeirdChars = L'z' - L'a' + L'9' - L'0' + 1;
				switch(pattern[i]){
				case L'ù': pos++;
				case L'ò': pos++;
				case L'ì': pos++;
				case L'é': pos++;
				case L'è': pos++;
				case L'à': pos += offsetWeirdChars;
					break;
				}
			}

			if(mapCH2N[pos] && mapCH2N[pos] != m_data[i])
				return false;

			mapCH2N[pos] = m_data[i];
		}

		return true;
	}

protected:
	unsigned int m_len;
	unsigned char *m_data;
};

	static const CryptoText rawData[] =
	/*{	5,	17,	5,	15,	23,	9,	20,	22,	11,	26,	19,	21,	20,	18,	12,	18,	14,	18,	10,
		8,	18,	24,	3,	19,	4,	28,	23,	11,	5,	20,	13,	19,	13,	13,	22,	22,	7,	26,
		6,	14,	10,	23,	29,	23,	22,	28,	15,	13,	5,	9,	7,	6,	22,	9,	14,	8,	21,
		20,	28,	17,	14,	19,	18,	7,	9,	17,	9,	16,	7,	17,	17,	6,	5,	26,	26,	6,
		9,	21,	14,	7,	9,	22,	8,	14,	7,	8,	19,	17,	13,	23,	27,	15,	13,	17,	27,
		18,	18,	17,	8,	11,	7,	9,	6,	8,	10,	15,	19,	25,	14,	21,	14,	13,	15,	4,
		5,	18,	19,	13,	8,	9,	11,	22,	11,	11,	8,	19,	23,	7,	20,	19,	5,	9,	29,
		10,	10,	22,	18,	19,	19,	19,	17,	24,	14,	11,	5,	13,	15,	20,	15,	22,	1,	11,
		14,	21,	11,	15,	5,	28,	25,	10,	16,	9,	14,	17,	23,	5,	10,	18,	14,	10,	27,
		26,	9,	10,	22,	18,	17,	17,	9,	9,	18,	14,	7,	23,	18,	14,	30,	14,	26,	16,
		22,	22,	5,	9,	22,	14,	19,	14,	19,	8,	2,	16,	23,	13,	9,	21,	13,	16,	0};
*/
		{5,	8,	6,	20,	9,	18,	5,	10,	14,	26,	22,
		17,	18,	14,	28,	21,	18,	18,	10,	21,	9,	22,
		5,	24,	10,	17,	14,	17,	19,	22,	11,	10,	5,
		15,	3,	23,	14,	7,	8,	13,	18,	15,	22,	9,
		23,	19,	29,	19,	9,	11,	8,	19,	5,	18,	22,
		9,	4,	23,	18,	22,	7,	9,	19,	28,	17,	14,
		20,	28,	22,	7,	8,	9,	11,	19,	25,	17,	19,
		22,	23,	28,	9,	14,	6,	22,	17,	10,	9,	14,
		11,	11,	15,	17,	7,	8,	11,	24,	16,	9,	19,
		26,	5,	13,	9,	8,	10,	11,	14,	9,	18,	8,
		19,	20,	5,	16,	19,	15,	8,	11,	14,	14,	2,
		21,	13,	9,	7,	17,	19,	19,	5,	17,	7,	16,
		20,	19,	7,	17,	13,	25,	23,	13,	23,	23,	23,
		18,	13,	6,	17,	23,	14,	7,	15,	5,	18,	13,
		12,	13,	22,	6,	27,	21,	20,	20,	10,	14,	9,
		18,	22,	9,	5,	15,	14,	19,	15,	18,	30,	21,
		14,	22,	14,	26,	13,	13,	5,	22,	14,	14,	13,
		18,	7,	8,	26,	17,	15,	9,	1,	10,	26,	16,
		10,	26,	21,	6,	27,	4,	29,	11,	27,	16,	0};

Problem::Problem() {
	memcpy(txtEnigma, rawData, sizeof(txtEnigma));
}

Problem::~Problem() {
}

vector<int> Problem::GetOccurencesNum(const wchar_t *testString){
	int patternLength = wcslen(testString);

	vector<CryptoString> strings(strlen((const char *)txtEnigma) - patternLength);
	vector<int> matchResult;

	vector<CryptoString>::iterator item = strings.begin();
	int pos = 0;

	while(item != strings.end()){
		item->Init(&txtEnigma[pos], patternLength);

		key problemMap;
		if(item->PatternMatching(testString, problemMap))
			matchResult.push_back(pos);
		pos++;
		item++;
	}

	return matchResult;
}

std::vector<key> Problem::GetOccurencesMaps(const wchar_t *testPatternString, std::vector<key> inputKeyList){
	int testPatternLength = wcslen(testPatternString);

	vector<CryptoString> candidateTokenList(strlen((const char *)txtEnigma) - testPatternLength);
	vector<key> matchResult;

	vector<CryptoString>::iterator candidateToken = candidateTokenList.begin();
	int txtPos = 0;

	//input map vuota
	if(inputKeyList.empty()){
		key nullKeyInitializer;
		inputKeyList.push_back(nullKeyInitializer);
	}

	while(candidateToken != candidateTokenList.end()){
		candidateToken->Init(&txtEnigma[txtPos], testPatternLength);

		vector<key>::iterator inputKey = inputKeyList.begin();

		while(inputKey != inputKeyList.end()){
			key currentKey(*inputKey);
			if(candidateToken->PatternMatching(testPatternString, currentKey)){
				matchResult.push_back(currentKey);
			}
			inputKey++;
		}
		txtPos++;
		candidateToken++;
	}

	return matchResult;
}


wchar_t *Problem::Translate(key map){
	size_t solutionLength = strlen((const char *)txtEnigma);
	wchar_t *solution = new wchar_t[solutionLength + 1];

	size_t i;
	for(i=0; i < solutionLength; i++){
		solution[i] = map[txtEnigma[i]] ? map[txtEnigma[i]] : L'.';
	}
	solution[i] = 0;

	return solution;
}

////////////////////////////////

Problem_Test::Problem_Test(){
	m_problem = new Problem();
}

Problem_Test::~Problem_Test(){
	delete m_problem;
}

void Problem_Test::Run(){
	const wchar_t *testWord[] = {
		L"chiunque",
		L"fascismo",
		L"prendesti",
		L"artistico",
		L"tuchestai",
	};

	int sizeOfTest = sizeof(testWord)/sizeof(testWord[0]);

	vector<int> occ[sizeOfTest];
	for(int i = 0; i < sizeOfTest; i++){
		printf("\nsearching for %ls...", testWord[i]);
		occ[i] = m_problem->GetOccurencesNum(testWord[i]);
		vector<int>::iterator j;
		for(j = occ[i].begin(); j != occ[i].end(); j++)
			printf("\n\t%d", *j);
	}
}
