/*
 * Problem.h
 *
 *  Created on: 24/mar/2016
 *      Author: pego
 */

#ifndef PROBLEM_H_
#define PROBLEM_H_

#include <vector>

typedef unsigned char CryptoText;
#define	MATRIX_SIZE	31
struct key{
	key(){
		for(int _i=0; _i<MATRIX_SIZE; _i++)
			x[_i] = 0;
	}
	wchar_t x[MATRIX_SIZE];
	wchar_t &operator[](std::size_t idx){ return x[idx]; }
};

class Problem {
public:
	Problem();
	virtual ~Problem();

	std::vector<int> GetOccurencesNum(const wchar_t *testString);
	std::vector<key> GetOccurencesMaps(const wchar_t *testString, std::vector<key> inputMapList);
	wchar_t *Translate(key map);

protected:
	CryptoText txtEnigma[19*11];
};


//UNIT TEST
class Problem_Test{
public:
	union{
		struct{
			unsigned char	test1 : 1,
							test2 : 1,
							test3 : 1,
							test4 : 1;
		};
		unsigned char b;
	}result;

	Problem_Test();
	~Problem_Test();

	void Run();

protected:
	Problem *m_problem;
};

#endif /* PROBLEM_H_ */

