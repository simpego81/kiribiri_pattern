//============================================================================
// Name        : kiribiri_pattern.cpp
// Author      : pego
// Version     :
// Copyright   : 1911
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>
#include "Problem.h"
using namespace std;

int main() {
	Problem *problem = new Problem();
	vector<int> occurences;
	wchar_t inString[100];

	do{
		vector<key> maps;
		do{
			std::wcin >> inString;

			maps = problem->GetOccurencesMaps(inString, maps);
			int nsol=0;
			for(vector<key>::iterator it = maps.begin(); it != maps.end(); it++, nsol++){
				wchar_t *solution = problem->Translate(*it);
				printf("\nsolution[%d]:\n\t", nsol);

				int ic = 0;
				wchar_t *solutionIterator = solution;
				while(*solutionIterator)
					printf((ic++ % 19) ? "%lc" : "\n\t%lc", *solutionIterator++);
				delete [] solution;
			}
		}while(inString[0] != '0');
	}while(inString[0] != L'x');

	return 0;
}
