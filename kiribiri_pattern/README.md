# KIRIBIRI PATTERN FINDER

This project is trying to find a solution of this mystery:

![Kiribiri - Frontespizio de "La Moglie Di Mia Moglie"](https://drive.google.com/file/d/0B5SfPlutHRB1ajNHcTBIMjFNQkk/preview)

## Description of the mystery

That's the frontispiece of an old italian book of novels. The author is the mysterious Tullio Alpinolo Bracci known as Kiribiri, and he wrote this masterpiece in 1927.
As you can see, it's a (strange!) matrix, 19 rows and 11 columns, filled with numbers with range 1 to 30. The last symbol is a question mark.
I think this is an encrypted text, because the so called "frequency analysis" shows a typical distribution of an italian text...
The problem is that there are 30 symbols (in spite of 21 or 26 letters - with non italian letters: "JKWXY"). So I think he put accented letters or numbers as well.

## Coincidences

* matrix is 19 x 11.
   An automobiles and aircraft manufacturer called "Chiribiri" was created in Turin, Italy on 1911: the author, close to a Futuristic sub-movement (they loved machines, cars and aircraft as well as war!) born on Turin in 1924, choose the pseudonym "Kiribiri".